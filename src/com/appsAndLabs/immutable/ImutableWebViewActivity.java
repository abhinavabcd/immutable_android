package com.appsAndLabs.immutable;

import java.util.logging.Logger;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class ImutableWebViewActivity extends Activity {
    private WebView mainWebView;
	private String deviceId;
	private static SharedPreferences preferences;
	
	private final static Logger LOG = Logger.getLogger(ImutableWebViewActivity.class.getName());
	private static final String WEB_ADDRESS = "http://192.168.1.6:8585";
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        
        
        this.mainWebView = (WebView) findViewById(R.id.mainWebView);
        mainWebView.setVisibility(View.INVISIBLE); 
        WebSettings webSettings = mainWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mainWebView.setWebViewClient(new MyCustomWebViewClient());
        mainWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        
		mainWebView.setVisibility(View.VISIBLE);
		mainWebView.loadUrl(WEB_ADDRESS+"/init/?authKey="+LoadingAndAuthentication.authKey	);
    	

        //Toast.makeText(this, deviceId, Toast.LENGTH_SHORT).show();
    }
	    
    private class MyCustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    
    
    
    
}

