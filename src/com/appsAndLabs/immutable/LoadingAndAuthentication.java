package com.appsAndLabs.immutable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;

import com.appsAndLabs.immutable.ImutableWebViewActivity;

public class LoadingAndAuthentication extends Activity {

	private EditText userIdInput;
	private TextView userInputLabel;

	private String deviceId;
	private static SharedPreferences preferences;

	public static String authKey=null;
	private static final String WEB_ADDRESS = "http://192.168.1.6:8585";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loading_and_authentication);
		 
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.userIdInput = (EditText) findViewById(R.id.editTextUserIdInput);
        this.userInputLabel = (TextView)findViewById(R.id.textViewenterName);
        userIdInput.setVisibility(View.INVISIBLE);
        userInputLabel.setVisibility(View.INVISIBLE);
        
        TextView.OnEditorActionListener userIdListener = new TextView.OnEditorActionListener(){
     		@Override
			public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
     		   if (actionId == EditorInfo.IME_NULL  
         		      && event.getAction() == KeyEvent.ACTION_DOWN) {
     			   		authenticateUser(textView.getText().toString());
         		   }
         		   return true;
     		}
        };
        //mainWebView.loadUrl("http://192.168.1.6:8585");
        userIdInput.setOnEditorActionListener(userIdListener);
        authenticateUser(getPreference("userId", null));
    

        //Toast.makeText(this, deviceId, Toast.LENGTH_SHORT).show();
    }
	
    private boolean authenticateUser(String userId){
        String deviceId =  getDeviceId();
        if(userId!=null){
        	//show progressing screen
        	DownloadWebPageTask task = new DownloadWebPageTask();
            task.execute(new String[] { userId, WEB_ADDRESS+"/init/"+deviceId+"/"+userId });
            return true;
         }
        
        //show user input here 
        	showUserNameInput();
        	return false;
    }
    
    private boolean navigateToWebView(String userId, String authKey){
    	if(!authKey.trim().equalsIgnoreCase("")){
				setPreference("userId", userId);
				LoadingAndAuthentication.authKey= authKey; 
				gotoMainScreen();
				return true;
		}
    	return false;
    }
    
    private String getDeviceId(){
        if(deviceId==null){
	    	deviceId = Secure.getString(this.getContentResolver(),
	                Secure.ANDROID_ID);
	    	//if(deviceId==null)
	    		
        }
    	return deviceId;
    }
    
    public static void setPreference(String key, String value) {
        SharedPreferences prefs = preferences;
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getPreference(String key , String defaultValue) {
        return preferences.getString(key, null);
    }
    
    public String getUrlResponse(String url) throws IOException{

        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        HttpResponse response = httpclient.execute(httpget);
        BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer("");
        String line = "";
        String NL = System.getProperty("line.separator");
        while ((line = in.readLine()) != null) {                    
            sb.append(line + NL);
        }
        in.close();
        String result = sb.toString();
		return result;
    }
    
    
    //async task
    
    private class DownloadWebPageTask extends AsyncTask<String, String, String> {
        private String key;

		@Override
        protected String doInBackground(String... data) {
        	String response = "";
            this.key = data[0];
        	String url = data[1];
        	try {
				return getUrlResponse(url);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	return null;
        }
        
        @Override
        protected void onPostExecute(String data) {
        	if(data==null){
        		// show error msg could not authenticate or there is a server error
        		return;
        	}
        	navigateToWebView(key , data.trim());
        }
      }

	
	protected boolean onUserIdEntered(String userId){
		return false;
		
	}
	
	protected void showUserNameInput(){
		userIdInput.setVisibility(View.VISIBLE);
        userInputLabel.setVisibility(View.VISIBLE);
	}
	
	protected void hideUserNameInput(){
		userIdInput.setVisibility(View.INVISIBLE);
        userInputLabel.setVisibility(View.INVISIBLE);
	}
	
	protected void gotoMainScreen(){
        Intent mainIntent = new Intent(LoadingAndAuthentication.this,ImutableWebViewActivity.class);
        LoadingAndAuthentication.this.startActivity(mainIntent);
        LoadingAndAuthentication.this.finish();
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.loading_and_authentication, menu);
		return true;
	}

	

/*
public class Splash extends Activity {

    private final int SPLASH_DISPLAY_LENGHT = 3000;            //set your time here......

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent mainIntent = new Intent(Splash.this,MainActivity.class);
                Splash.this.startActivity(mainIntent);
                Splash.this.finish();
            }
        }, SPLASH_DISPLAY_LENGHT);
    }
}
*/

	
}
